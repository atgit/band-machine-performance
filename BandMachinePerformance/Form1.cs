﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace BandMachinePerformance
{
   
    public partial class Form1 : Form
    {
        DateTime shiftStart;
        DBService db = new DBService();
        DbServiceSQL dbSQL=new DbServiceSQL();
        int BreakParameter = 5;
        int defaultValue = 1; //ישמש במקרה שלא היית עבודה בכלל
        bool changedShift = false;
        bool firstTime = true;
        public Form1()
        {
            InitializeComponent();
            initShift();
            styleChart();
            addValuesToChart();
            calcEfficiency();
            updateReport();
            timer2.Start();
            timer1.Start();
        }

        void styleChart()
        {
            WindowState = FormWindowState.Maximized;
            breakTimeChart.ChartAreas[0].AxisX.Interval = 15;
            breakTimeChart.ChartAreas[0].AxisY.Interval = 1;
            breakTimeChart.ChartAreas[0].AxisX.Minimum = 0;
            breakTimeChart.ChartAreas[0].AxisX.LabelStyle.Angle = -90;
            Axis axisY = breakTimeChart.ChartAreas[0].AxisY;
            axisY.CustomLabels.Add(0.5, 1, "עבודה");
            axisY.CustomLabels.Add(0, 0.5, "אי עבודה");
            axisY.LabelStyle.Font = new Font("Arial", 15, FontStyle.Bold);
            Axis axisX = breakTimeChart.ChartAreas[0].AxisX;
            axisX.LabelStyle.Font = new Font("Arial", 15, FontStyle.Bold);
            axisY.Maximum = 1.01;


            
        }

        void calcEfficiency()
        {
            string QUERY = $@"select COUNT(*) from tapiali.spadon
                                WHERE STIMESTAMP BETWEEN '{shiftStart.ToString("yyyy-MM-dd HH:mm")}:00.000000' AND '{shiftStart.AddMinutes(8.5 * 60).ToString("yyyy-MM-dd HH:mm")}:00.000000'
                                AND SMACH='40'
                                AND SDEPT='57'";
            double howMuchWorked = double.Parse(db.executeSelectQueryNoParam(QUERY).Rows[0].ItemArray[0].ToString());
            double howMuchTimeAtShift =(DateTime.Now - shiftStart).TotalMinutes;
            double precent = howMuchWorked * 100 / howMuchTimeAtShift;
            efficiencyLabel.Text = precent.ToString("0.##") + "%";
            if(precent > 70)
            {
                efficiencyLabel.BackColor = Color.Green;
            }
            else if (precent > 50)
            {
                efficiencyLabel.BackColor = Color.Orange;
            }
            else if (precent > 35)
            {
                efficiencyLabel.BackColor = Color.Red;
            }
            else
            {
                efficiencyLabel.BackColor = Color.Black;
            }
            
        }

        void initShift()
        {
            int shift = db.GetShiftNum();
            if (checkIfWeekEndShift())
            {
                initShiftStartWeekEnd();
                return;
            }
            if (shift == 1)
            {
                if (DateTime.Now.Hour == 23)
                    shiftStart = DateTime.ParseExact(DateTime.Now.ToShortDateString() + " 23:30", "dd/MM/yyyy HH:mm",
                                              System.Globalization.CultureInfo.InvariantCulture);
                else shiftStart = DateTime.ParseExact(DateTime.Now.AddDays(-1).ToShortDateString() + " 23:30", "dd/MM/yyyy HH:mm",
                                          System.Globalization.CultureInfo.InvariantCulture);
            }
            else if (shift == 2)
            {
              
                shiftStart = DateTime.ParseExact(DateTime.Now.ToShortDateString() + " 06:30", "dd/MM/yyyy HH:mm",
                                          System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                shiftStart = DateTime.ParseExact(DateTime.Now.ToShortDateString() + " 15:00", "dd/MM/yyyy HH:mm",
                                          System.Globalization.CultureInfo.InvariantCulture);
            }


        }

        private void initShiftStartWeekEnd()
        {
            DateTime now = DateTime.Now;
            if (now.Hour < 06 || (now.Hour == 06 && now.Minute < 30))
            {
               
                shiftStart = DateTime.ParseExact(DateTime.Now.ToShortDateString() + " 18:30", "dd/MM/yyyy HH:mm",
                                              System.Globalization.CultureInfo.InvariantCulture);
                shiftStart = shiftStart.AddDays(-1);
                shiftLabel.Text = "משמרת ערב-12 שעות";
            }
            if ((now.Hour == 18 && now.Minute > 30) || now.Hour > 18)
            {
                shiftStart = DateTime.ParseExact(DateTime.Now.ToShortDateString() + " 18:30", "dd/MM/yyyy HH:mm",
                                              System.Globalization.CultureInfo.InvariantCulture);
                shiftLabel.Text = "משמרת ערב-12 שעות";
            }
            if ((now.Hour == 06 && now.Minute > 30) || (now.Hour > 06 && now.Hour < 18) || (now.Hour == 18 && now.Minute < 30))
            {
                shiftLabel.Text = "משמרת בוקר-12 שעות";
                shiftStart = DateTime.ParseExact(DateTime.Now.ToShortDateString() + " 06:30", "dd/MM/yyyy HH:mm",
                                              System.Globalization.CultureInfo.InvariantCulture);
            }
        }

        void updateShiftStart()
        {

            int shift = db.GetShiftNum();
            if (checkIfWeekEndShift())
            {
                updateShiftStartWeekEnd();
                return;
            }
            if (shift == 1)
            {
                if (shiftStart.Hour != 23)
                    changedShift = true;
                if (DateTime.Now.Hour == 23)
                    shiftStart = DateTime.ParseExact(DateTime.Now.ToShortDateString() + " 23:30", "dd/MM/yyyy HH:mm",
                                              System.Globalization.CultureInfo.InvariantCulture);
                else shiftStart = DateTime.ParseExact(DateTime.Now.AddDays(-1).ToShortDateString() + " 23:30", "dd/MM/yyyy HH:mm",
                                          System.Globalization.CultureInfo.InvariantCulture);
            }
            else if (shift == 2)
            {
                if (shiftStart.Hour != 06)
                    changedShift = true;
                shiftStart = DateTime.ParseExact(DateTime.Now.ToShortDateString() + " 06:30", "dd/MM/yyyy HH:mm",
                                          System.Globalization.CultureInfo.InvariantCulture);
            }
            else
            {
                if (shiftStart.Hour != 15)
                    changedShift = true;
                shiftStart = DateTime.ParseExact(DateTime.Now.ToShortDateString() + " 15:00", "dd/MM/yyyy HH:mm",
                                          System.Globalization.CultureInfo.InvariantCulture);
            }



        }

        private void updateShiftStartWeekEnd()
        {
            DateTime now = DateTime.Now;
            if(now.Hour<06 || (now.Hour==06 && now.Minute < 30))
            {
                if (shiftStart.Hour != 18 || shiftStart.Minute!=30)
                    changedShift = true;
                shiftStart = DateTime.ParseExact(DateTime.Now.ToShortDateString() + " 18:30", "dd/MM/yyyy HH:mm",
                                              System.Globalization.CultureInfo.InvariantCulture);
                shiftStart=shiftStart.AddDays(-1);
                shiftLabel.Text = "משמרת ערב-12 שעות";
            }
            if ((now.Hour == 18 && now.Minute > 30) || now.Hour > 18)
            {
                if (shiftStart.Hour != 18 || shiftStart.Minute != 30)
                    changedShift = true;
                shiftStart = DateTime.ParseExact(DateTime.Now.ToShortDateString() + " 18:30", "dd/MM/yyyy HH:mm",
                                              System.Globalization.CultureInfo.InvariantCulture);
                shiftLabel.Text = "משמרת ערב-12 שעות";
            }
            if ((now.Hour == 06 && now.Minute > 30) || (now.Hour > 06 && now.Hour < 18) || (now.Hour == 18 && now.Minute < 30))
            {
                if (shiftStart.Hour != 06)
                    changedShift = true;
                shiftLabel.Text = "משמרת בוקר-12 שעות";
                shiftStart = DateTime.ParseExact(DateTime.Now.ToShortDateString() + " 06:30", "dd/MM/yyyy HH:mm",
                                              System.Globalization.CultureInfo.InvariantCulture);
            }

        }

        bool checkIfWeekEndShift()
        {
            DateTime now = DateTime.Now;
            if (now.DayOfWeek == DayOfWeek.Saturday)
                return true;
            if (now.DayOfWeek == DayOfWeek.Friday && now.Hour > 15 )
                return true;
            if (now.DayOfWeek == DayOfWeek.Sunday && (now.Hour < 06 ||now.Hour==06 && now.Minute < 30))
                return true;
            return false;
        }
        void addValuesToChart()
        {
            foreach(DataPoint p in breakTimeChart.Series[0].Points)
            {
                p.Dispose();
            }
            breakTimeChart.Series[0].Points.Clear();
            updateShiftStart();
            DateTime d = shiftStart;
            for (int i = 0; i < (DateTime.Now - shiftStart).TotalMinutes; i++)
            {
                breakTimeChart.Series[0].Points.AddXY(i, defaultValue);
                breakTimeChart.Series[0].Points[i].AxisLabel = d.AddMinutes(i).ToShortTimeString();
            }
            checkForBreaks();
            
        }


        void checkForBreaks()
        {
            addBreak(0, 1);
            string QUERY = $@"select STIMESTAMP from tapiali.spadon
                                WHERE STIMESTAMP BETWEEN '{shiftStart.ToString("yyyy-MM-dd HH:mm")}:00.000000' AND '{shiftStart.AddMinutes(8.5*60).ToString("yyyy-MM-dd HH:mm")}:00.000000'
                                AND SMACH='40'
                                AND SDEPT='57'";
            DataTable data=db.executeSelectQueryNoParam(QUERY);
            List<DateTime> forCalcBreaks = new List<DateTime>();
            forCalcBreaks.Add(shiftStart);
            foreach(DataRow row in data.Rows)
            {
                forCalcBreaks.Add(DateTime.ParseExact(row.ItemArray[0].ToString().Substring(0,16), "yyyy-MM-dd-HH.mm",
                                       System.Globalization.CultureInfo.InvariantCulture));
            }
            //מתחילת המשמרת לא הוזן כלום
            if (forCalcBreaks.Count == 1)
            {
                if (defaultValue == 0) return;
                defaultValue = 0;
                addValuesToChart();
                return;
            }
            forCalcBreaks.Add(DateTime.Now);//אם קורה מצב שבין המצב האחרון ועד עכשיו יש הפסקה גדולה מהפרמטר
            defaultValue = 1;
            for(int i = 0; i < forCalcBreaks.Count - 1; i++)
            {
                if ((forCalcBreaks[i + 1] - forCalcBreaks[i]).TotalMinutes > BreakParameter)
                {
                    addBreak((int)(forCalcBreaks[i] - shiftStart).TotalMinutes, (int)(forCalcBreaks[i + 1] - forCalcBreaks[i]).TotalMinutes);
                }
            }

            breakTimeChart.Series[0].Points[breakTimeChart.Series[0].Points.Count-1].SetValueY(0);

        }
        void addBreak(int minuteInTheShift,int breakTimeInMinutes)
        {
            if(minuteInTheShift==0)
                breakTimeChart.Series[0].Points[0].SetValueY(0);
            for (int i = minuteInTheShift+1; i < minuteInTheShift+breakTimeInMinutes; i++)
            {
                try
                {
                    breakTimeChart.Series[0].Points[i].SetValueY(0);
                    //breakTimeChart.Series[1].Points.AddXY(i, 1);
                    
                }
                catch(Exception e) { }
            }
        }
     


        private void timer1_Tick(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab.Name == "graphPage")
                tabControl1.SelectedIndex = 0;
            else tabControl1.SelectedIndex = 1;
        }

        

        void updateReport()
        {
            string query;
            if (changedShift && !firstTime)
            {
                deleteDataFromDB();
                employeeNameLabel.Text = "";
                changedShift = false;
            }
            firstTime = false;
            int shiftCode;
            switch (shiftStart.Hour)
            {
                case 06: shiftLabel.Text = "משמרת בוקר";shiftCode=2; break;
                case 15: shiftLabel.Text = "משמרת ערב"; shiftCode = 3; break;
                case 23: shiftLabel.Text = "משמרת לילה"; shiftCode = 1; break;
            }
            upToLabel.Text = "מעודכן ל:" + DateTime.Now.ToLongTimeString();
             query = $@"SELECT SerialNo as 'מקט','' as 'גודל', PlanQty as 'כמות תכנון',ActualQty as 'כמות ביצוע' 
                              FROM dbo.Machines_Plan_Actual";
            reportGridView.DataSource = dbSQL.executeSelectQueryNoParam(query);
            foreach (DataGridViewColumn c in reportGridView.Columns)
                c.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            reportGridView.AutoSize = true;
            efficiencyLabel.Location = new Point(efficiencyLabel.Location.X, reportGridView.Location.Y + reportGridView.Height + 5);
            efficiencyLabel.Size = new Size(200, 100);
            query= $@"SELECT Machine,EmployeeNo 
                      FROM dbo.Machines_Plan_Actual";
            DataTable data = dbSQL.executeSelectQueryNoParam(query);
            try //אם אין שורות יזרוק שגיאה
            {
                employeeNameLabel.Text = "עובד: " + getNameByEmpNumber(int.Parse(data.Rows[0]["EmployeeNo"].ToString()));
                machineLabel.Text = "מכונה: " + data.Rows[0]["Machine"];
            }
            catch (Exception e) { }
            updateSizeInTable();
            updateTotalAmountAtShift();
            
        }

        private void updateTotalAmountAtShift()
        {
            int count = 0;
            foreach (DataGridViewRow row in reportGridView.Rows)
                count += int.Parse(row.Cells[3].Value.ToString());
            howManyLabel.Text = "סה\"כ מתחילת משמרת:" + count;
            
        }

        private void deleteDataFromDB()
        {
            string query = $@"DELETE 
                            FROM dbo.Machines_Plan_Actual";
            dbSQL.ExecuteQuery(query, CommandType.Text); 
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            addValuesToChart();
            calcEfficiency();
            updateReport();
            Console.WriteLine("Ticked");
        }

        private string getNameByEmpNumber(int emp)
        {
            string query = $@"select PRATI,FAMILY
                              from ISUFKV.ISAVL10
                              WHERE OVED='00{emp}'";
            string toReturn = "";
            DataTable data=db.executeSelectQueryNoParam(query);
            try
            {
                toReturn = data.Rows[0]["PRATI"].ToString() + " " + data.Rows[0]["FAMILY"].ToString();
            }
            catch(Exception e) { }
            return toReturn;
        }
        private void updateSizeInTable()
        {
            
            
            foreach(DataGridViewRow row in reportGridView.Rows)
            {
                string query = $@"SELECT INSIZ
                                FROM bpcsfali.iimnl01                                WHERE INPROD='{row.Cells[0].Value.ToString().Split('-')[0]}'";
                try
                {
                    row.Cells[1].Value = db.executeSelectQueryNoParam(query).Rows[0].ItemArray[0];
                }
                catch(Exception e) { }
                
            }
           
            
        }

     
    }
}
