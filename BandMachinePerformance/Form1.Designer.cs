﻿namespace BandMachinePerformance
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tablePage = new System.Windows.Forms.TabPage();
            this.breakTimeChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.graphPage = new System.Windows.Forms.TabPage();
            this.howManyLabel = new System.Windows.Forms.Label();
            this.machineLabel = new System.Windows.Forms.Label();
            this.employeeNameLabel = new System.Windows.Forms.Label();
            this.upToLabel = new System.Windows.Forms.Label();
            this.shiftLabel = new System.Windows.Forms.Label();
            this.reportGridView = new System.Windows.Forms.DataGridView();
            this.efficiencyLabel = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.tabControl1.SuspendLayout();
            this.tablePage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.breakTimeChart)).BeginInit();
            this.graphPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.AccessibleName = "";
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tablePage);
            this.tabControl1.Controls.Add(this.graphPage);
            this.tabControl1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.tabControl1.Location = new System.Drawing.Point(3, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1572, 642);
            this.tabControl1.TabIndex = 0;
            // 
            // tablePage
            // 
            this.tablePage.AccessibleName = "";
            this.tablePage.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tablePage.BackgroundImage")));
            this.tablePage.Controls.Add(this.breakTimeChart);
            this.tablePage.Location = new System.Drawing.Point(4, 31);
            this.tablePage.Name = "tablePage";
            this.tablePage.Padding = new System.Windows.Forms.Padding(3);
            this.tablePage.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tablePage.Size = new System.Drawing.Size(1564, 607);
            this.tablePage.TabIndex = 0;
            this.tablePage.Text = "ביצועי מכונה";
            this.tablePage.UseVisualStyleBackColor = true;
            // 
            // breakTimeChart
            // 
            this.breakTimeChart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea1.BackColor = System.Drawing.Color.White;
            chartArea1.BackSecondaryColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            chartArea1.Name = "ChartArea1";
            this.breakTimeChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.breakTimeChart.Legends.Add(legend1);
            this.breakTimeChart.Location = new System.Drawing.Point(65, 106);
            this.breakTimeChart.Name = "breakTimeChart";
            this.breakTimeChart.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            series1.BorderWidth = 0;
            series1.ChartArea = "ChartArea1";
            series1.CustomProperties = "PointWidth=1";
            series1.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series1.IsVisibleInLegend = false;
            series1.LabelBorderWidth = 0;
            series1.Legend = "Legend1";
            series1.MarkerBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            series1.MarkerBorderWidth = 0;
            series1.MarkerSize = 20;
            series1.Name = "ביצועי מכונות";
            series1.YValuesPerPoint = 2;
            this.breakTimeChart.Series.Add(series1);
            this.breakTimeChart.Size = new System.Drawing.Size(1240, 370);
            this.breakTimeChart.TabIndex = 0;
            // 
            // graphPage
            // 
            this.graphPage.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("graphPage.BackgroundImage")));
            this.graphPage.Controls.Add(this.howManyLabel);
            this.graphPage.Controls.Add(this.machineLabel);
            this.graphPage.Controls.Add(this.employeeNameLabel);
            this.graphPage.Controls.Add(this.upToLabel);
            this.graphPage.Controls.Add(this.shiftLabel);
            this.graphPage.Controls.Add(this.reportGridView);
            this.graphPage.Controls.Add(this.efficiencyLabel);
            this.graphPage.Location = new System.Drawing.Point(4, 31);
            this.graphPage.Name = "graphPage";
            this.graphPage.Padding = new System.Windows.Forms.Padding(3);
            this.graphPage.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.graphPage.Size = new System.Drawing.Size(1564, 607);
            this.graphPage.TabIndex = 1;
            this.graphPage.Text = "דו\"ח ביצוע";
            this.graphPage.UseVisualStyleBackColor = true;
            // 
            // howManyLabel
            // 
            this.howManyLabel.AutoSize = true;
            this.howManyLabel.BackColor = System.Drawing.Color.LightYellow;
            this.howManyLabel.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.howManyLabel.ForeColor = System.Drawing.Color.Red;
            this.howManyLabel.Location = new System.Drawing.Point(466, 28);
            this.howManyLabel.Name = "howManyLabel";
            this.howManyLabel.Size = new System.Drawing.Size(0, 33);
            this.howManyLabel.TabIndex = 6;
            // 
            // machineLabel
            // 
            this.machineLabel.AutoSize = true;
            this.machineLabel.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.machineLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.machineLabel.Location = new System.Drawing.Point(1546, 28);
            this.machineLabel.Name = "machineLabel";
            this.machineLabel.Size = new System.Drawing.Size(0, 33);
            this.machineLabel.TabIndex = 5;
            // 
            // employeeNameLabel
            // 
            this.employeeNameLabel.AutoSize = true;
            this.employeeNameLabel.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.employeeNameLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.employeeNameLabel.Location = new System.Drawing.Point(1193, 28);
            this.employeeNameLabel.Name = "employeeNameLabel";
            this.employeeNameLabel.Size = new System.Drawing.Size(0, 33);
            this.employeeNameLabel.TabIndex = 4;
            // 
            // upToLabel
            // 
            this.upToLabel.AutoSize = true;
            this.upToLabel.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.upToLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.upToLabel.Location = new System.Drawing.Point(191, 28);
            this.upToLabel.Name = "upToLabel";
            this.upToLabel.Size = new System.Drawing.Size(0, 33);
            this.upToLabel.TabIndex = 3;
            // 
            // shiftLabel
            // 
            this.shiftLabel.AutoSize = true;
            this.shiftLabel.Font = new System.Drawing.Font("Arial", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.shiftLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.shiftLabel.Location = new System.Drawing.Point(851, 28);
            this.shiftLabel.Name = "shiftLabel";
            this.shiftLabel.Size = new System.Drawing.Size(0, 33);
            this.shiftLabel.TabIndex = 2;
            // 
            // reportGridView
            // 
            this.reportGridView.AllowUserToAddRows = false;
            this.reportGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reportGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.reportGridView.Location = new System.Drawing.Point(6, 113);
            this.reportGridView.Name = "reportGridView";
            this.reportGridView.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.reportGridView.Size = new System.Drawing.Size(1555, 464);
            this.reportGridView.TabIndex = 1;
            // 
            // efficiencyLabel
            // 
            this.efficiencyLabel.AutoSize = true;
            this.efficiencyLabel.Font = new System.Drawing.Font("Arial", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.efficiencyLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.efficiencyLabel.Location = new System.Drawing.Point(851, 540);
            this.efficiencyLabel.Name = "efficiencyLabel";
            this.efficiencyLabel.Size = new System.Drawing.Size(215, 75);
            this.efficiencyLabel.TabIndex = 0;
            this.efficiencyLabel.Text = "label1";
            // 
            // timer1
            // 
            this.timer1.Interval = 40000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 100000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1568, 626);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Band Machine Performance";
            this.tabControl1.ResumeLayout(false);
            this.tablePage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.breakTimeChart)).EndInit();
            this.graphPage.ResumeLayout(false);
            this.graphPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tablePage;
        private System.Windows.Forms.TabPage graphPage;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataVisualization.Charting.Chart breakTimeChart;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label efficiencyLabel;
        private System.Windows.Forms.Label upToLabel;
        private System.Windows.Forms.Label shiftLabel;
        private System.Windows.Forms.DataGridView reportGridView;
        private System.Windows.Forms.Label employeeNameLabel;
        private System.Windows.Forms.Label machineLabel;
        private System.Windows.Forms.Label howManyLabel;
    }
}

